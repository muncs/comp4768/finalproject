//
//  MyStashesTableViewController.m
//  finalproject
//
//  Created by Justin on 2018-11-26.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import "MyStashesTableViewController.h"
#import "AddStashViewController.h"
#import "Stash.h"
#import "DataProvider.h"


@interface MyStashesTableViewController ()

@property Stash *tappedStash;
@property NSMutableArray *My_stashes;

@end

@implementation MyStashesTableViewController

const float CELL_IMAGE_HEIGHT = 90;
const float CELL_IMAGE_WIDTH = 90;
const float CELL_TEXT_HEIGHT = 30;
const float CELL_TEXT_WIDTH = 200;
const float CELL_TEXT_OFFSET = 10;
const float CELL_TEXT_TITLE_SIZE = 20;
const float CELL_TEXT_SIZE = 16;


- (void)viewDidLoad {
    [super viewDidLoad];
    [[DataProvider getInstance] getStashes:^void(NSMutableArray *stashes) {
        _My_stashes = [[NSMutableArray alloc] initWithArray:stashes copyItems:YES];
    }];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.My_stashes count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listPrototypeCell" forIndexPath:indexPath];

    Stash *theStash = [self.My_stashes objectAtIndex:indexPath.row];

    //Create subviews
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0,
            0,
            CELL_IMAGE_WIDTH,
            CELL_IMAGE_HEIGHT)];
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
            CELL_IMAGE_HEIGHT * 0,
            [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
            CELL_TEXT_HEIGHT)];
    UILabel *difficulty = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
            CELL_IMAGE_HEIGHT * (1.0 / 3.0),
            [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
            CELL_TEXT_HEIGHT)];
    UILabel *accuracy = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
            CELL_IMAGE_HEIGHT * (2.0 / 3.0),
            [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
            CELL_TEXT_HEIGHT)];

    UILabel *coords = [[UILabel alloc] initWithFrame:CGRectMake(CELL_IMAGE_WIDTH + CELL_TEXT_OFFSET,
            CELL_IMAGE_HEIGHT * (2.0 / 3.0),
            [[UIScreen mainScreen] bounds].size.width - CELL_IMAGE_WIDTH,
            CELL_TEXT_HEIGHT)];

    //Configure subviews
    [name setFont:[UIFont boldSystemFontOfSize:CELL_TEXT_TITLE_SIZE]];


    //Populate subviews

    name.text = theStash.name;
    difficulty.text = theStash.difficulty;
    accuracy.text = theStash.accuracy;
    coords.text = theStash.coords;

    //Add subviews to cell
    // [cell addSubview:image];
    [cell addSubview:name];
    [cell addSubview:difficulty];
    [cell addSubview:accuracy];
    [cell addSubview:coords];


    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
