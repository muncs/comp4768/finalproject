//
//  AppDelegate.h
//  finalproject
//
//  Created by Devin on 2018-11-20.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocationProvider;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
