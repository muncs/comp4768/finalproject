//
//  MapViewController.h
//  finalproject
//
//  Created by Devin on 2018-11-20.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationListener.h"
#import "StashListener.h"


@interface MapViewController : UIViewController <LocationListener, StashListener> {
    GMSMapView *map;
    CLLocationCoordinate2D location;
}


@end
