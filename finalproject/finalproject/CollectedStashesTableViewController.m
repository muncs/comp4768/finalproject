//
//  MyStashesTableViewController.m
//  finalproject
//
//  Created by Justin on 2018-11-26.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import "MyStashesTableViewController.h"
#import "AddStashViewController.h"
#import "MyStashesDetailViewController.h"
#import "CollectedStashesTableViewController.h"

@interface CollectedStashesTableViewController ()

@end

@implementation CollectedStashesTableViewController

//Constants
const float COLLECTED_CELL_IMAGE_HEIGHT = 90;
const float COLLECTED_CELL_IMAGE_WIDTH = 90;
const float COLLECTED_CELL_TEXT_HEIGHT = 30;
const float COLLECTED_CELL_TEXT_OFFSET = 10;
const float COLLECTED_CELL_TEXT_TITLE_SIZE = 20;

//List of user's stashes
NSMutableArray *myCollectedStashList;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];

    //Get list of user's stashes
    [[DataProvider getInstance] getFoundStashesForUser:uniqueIdentifier callback:^(NSMutableArray *array) {
        for (Stash *data in array) {
            //Lazily initialize
            if (!myCollectedStashList) {
                myCollectedStashList = [[NSMutableArray alloc] init];
            }

            //Add new stashes
            if (![myCollectedStashList containsObject:data]) {
                [myCollectedStashList addObject:data];
            }
        }

        //Reload data
        [self.tableView reloadData];
    }];

    //Reload data
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"collectedStashDetails"]) {
        //Setup detail view
        MyStashesDetailViewController *viewController2 = [segue destinationViewController];
        Stash* stash = myCollectedStashList[(NSUInteger) [[[self tableView] indexPathForSelectedRow] row]];
        [viewController2 setStashModel:stash];
    }
}

- (IBAction)unwindToTable:(UIStoryboardSegue *)segue {
    [[self tableView] reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [myCollectedStashList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listPrototypeCell" forIndexPath:indexPath];

    Stash *stash = myCollectedStashList[(NSUInteger) indexPath.row];

    //Create subviews
    UIImageView *stashImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,
            0,
            COLLECTED_CELL_IMAGE_WIDTH,
            COLLECTED_CELL_IMAGE_HEIGHT)];
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(COLLECTED_CELL_IMAGE_WIDTH + COLLECTED_CELL_TEXT_OFFSET,
            COLLECTED_CELL_IMAGE_HEIGHT * 0,
            [[UIScreen mainScreen] bounds].size.width - COLLECTED_CELL_IMAGE_WIDTH,
            COLLECTED_CELL_TEXT_HEIGHT)];
    UILabel *difficulty = [[UILabel alloc] initWithFrame:CGRectMake(COLLECTED_CELL_IMAGE_WIDTH + COLLECTED_CELL_TEXT_OFFSET,
            COLLECTED_CELL_IMAGE_HEIGHT * (1.0 / 3.0),
            [[UIScreen mainScreen] bounds].size.width - COLLECTED_CELL_IMAGE_WIDTH,
            COLLECTED_CELL_TEXT_HEIGHT)];
    UILabel *accuracy = [[UILabel alloc] initWithFrame:CGRectMake(COLLECTED_CELL_IMAGE_WIDTH + COLLECTED_CELL_TEXT_OFFSET,
            COLLECTED_CELL_IMAGE_HEIGHT * (2.0 / 3.0),
            [[UIScreen mainScreen] bounds].size.width - COLLECTED_CELL_IMAGE_WIDTH,
            COLLECTED_CELL_TEXT_HEIGHT)];

    //Configure subviews
    [name setFont:[UIFont boldSystemFontOfSize:COLLECTED_CELL_TEXT_TITLE_SIZE]];

    [[DataProvider getInstance] getMediaForStash:stash callback:^(NSURL *url) {
        NSData *imageData = [[NSData alloc] initWithContentsOfURL:url];
        stashImage.image = [[UIImage alloc] initWithData:imageData];
    }];

    //Populate subviews
    difficulty.text = stash.difficulty;
    accuracy.text = stash.accuracy;
    name.text = stash.name;

    //Add subviews to cell
    [cell addSubview:stashImage];
    [cell addSubview:name];
    [cell addSubview:difficulty];
    [cell addSubview:accuracy];

    return cell;
}

@end