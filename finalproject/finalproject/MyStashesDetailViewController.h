//
//  MyStashesDetailViewController.h
//  finalproject
//
//  Created by Justin on 2018-12-09.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Stash.h"
#import "MyStashesTableViewController.h"
#import "DataProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyStashesDetailViewController : UIViewController <GMSMapViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *myStashesDetailPicture;
@property (weak, nonatomic) IBOutlet GMSMapView *myStashesDetailMap;

@property (weak, nonatomic) IBOutlet UITextField *myStashesDetailName;
@property (weak, nonatomic) IBOutlet UITextField *myStashesDetailDifficulty;
@property (weak, nonatomic) IBOutlet UITextField *myStashesDetailAccuracy;

@property Stash *stash;

-(void)setStashModel:(Stash*)stash;


@end

NS_ASSUME_NONNULL_END
