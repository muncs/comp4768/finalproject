//
//  CollectedStashesDetailViewController.h
//  finalproject
//
//  Created by Justin on 2018-12-09.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Stash.h"
#import "DataProvider.h"
NS_ASSUME_NONNULL_BEGIN

@interface CollectedStashesDetailViewController : UIViewController <GMSMapViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *collectedDetailMap;
@property (weak, nonatomic) IBOutlet UIImageView *collectedDetailPicture;
@property (weak, nonatomic) IBOutlet UITextField *collectedDetailName;
@property (weak, nonatomic) IBOutlet UITextField *collectedDetailDifficulty;
@property (weak, nonatomic) IBOutlet UITextField *collectedDetailAccuracy;

@property Stash *stash;

-(void)setStashModel:(Stash*)stash;
@end

NS_ASSUME_NONNULL_END
