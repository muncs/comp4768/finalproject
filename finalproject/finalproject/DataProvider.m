//
// Created by devin on 2018-11-21.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import "DataProvider.h"
#import "Stash.h"
#import "FIRFieldValue.h"
#import <FirebaseFirestore/FIRFirestore.h>
#import <FirebaseFirestore/FIRQuerySnapshot.h>
#import <FirebaseFirestore/FIRDocumentSnapshot.h>
#import <FirebaseFirestore/FIRCollectionReference.h>
#import <FirebaseFirestore/FIRDocumentReference.h>
#import <FirebaseStorage/FIRStorage.h>
#import <FirebaseStorage/FIRStorageReference.h>


@implementation DataProvider {

    //Handles stash uploads/downloads
    FIRFirestore *firestore;

    //Handles media  uploads/downloads
    FIRStorage *storage;

}

//Singleton
static DataProvider *instance = nil;

//Collections
static NSString * const COLLECTION_STASHES = @"stashes";

//List of listeners to be notified during certain stash updates
NSMutableArray *stashListeners;

+ (id)getInstance {
    if (!instance) {
        instance = [[DataProvider alloc] init];
    }

    return instance;
}

- (id)init {
    if (!instance) {
        instance = (DataProvider *) [super init];
        firestore = [FIRFirestore firestore];
        storage = [FIRStorage storage];
        stashListeners = [[NSMutableArray alloc] init];

        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(updateListeners) userInfo:nil repeats:YES];
    }

    return instance;
}

/**
 * Updates all StashListners
 */
- (void)updateListeners {
    [self getStashesWithCallback:^(NSMutableArray *stashes) {
        for (id <StashListener> listener in stashListeners) {
            [listener onStashesUpdate:stashes];
        }
    }];
}

/**
 * Registers a StashListener to DataProvider. Will get notified on certain stash updates
 * @param listener Listener to be notified
 */
- (void)registerStashListener:(id<StashListener>)listener {
    [stashListeners addObject:listener];
}

/**
 * Retrieves a list of all stashes
 * @param callback Completion callback
 */
- (void)getStashesWithCallback:(void (^)(NSMutableArray*))callback {
    [[firestore collectionWithPath:COLLECTION_STASHES] getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
        NSMutableArray *stashes = nil;
        if (error != nil) {
            NSLog(@"Error retrieving data");
        } else {
            stashes = [[NSMutableArray alloc] init];
            for (FIRDocumentSnapshot *document in snapshot.documents) {
                NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:document.data];
                data[@"id"] = document.documentID;
                [stashes addObject:data];
            }
        }

        callback([self parseDocuments:stashes]);
    }];
}

/**
 * Retrieves a list of stashes that the user has created
 * @param userId User to search for
 * @param callback Completion callback
 */
- (void)getStashesForUser:(NSString *)userId callback:(void (^)(NSMutableArray *))callback {
    [[[firestore collectionWithPath:COLLECTION_STASHES] queryWhereField:@"user_id" isEqualTo:userId] getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
        NSMutableArray *stashes = nil;
        if (error != nil) {
            NSLog(@"Error retrieving data for user ID: %@", userId);
        } else {
            stashes = [[NSMutableArray alloc] init];
            for (FIRDocumentSnapshot *document in snapshot.documents) {
                NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:document.data];
                data[@"id"] = document.documentID;
                [stashes addObject:data];
            }
        }

        callback([self parseDocuments:stashes]);
    }];
}

/**
 * Retrieves a list of stashe's that the user has already found
 * @param userId User to search for
 * @param callback Completion callback
 */
- (void)getFoundStashesForUser:(NSString *)userId callback:(void (^)(NSMutableArray *))callback {
    [[[firestore collectionWithPath:COLLECTION_STASHES] queryWhereField:@"founders_id" arrayContains:userId] getDocumentsWithCompletion:^(FIRQuerySnapshot *_Nullable snapshot, NSError *_Nullable error) {
        NSMutableArray *stashes = nil;
        if (error != nil) {
            NSLog(@"Error retrieving data for user ID: %@", userId);
        } else {
            stashes = [[NSMutableArray alloc] init];
            for (FIRDocumentSnapshot *document in snapshot.documents) {
                NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:document.data];
                if (data[@"user_id"] != userId) {
                    data[@"id"] = document.documentID;
                    [stashes addObject:data];
                }
            }
        }

        callback([self parseDocuments:stashes]);
    }];
}

/**
 * Add a new stash
 * @param stash Stash to add
 * @param callback Completion callback
 */
- (void)addStash:(Stash *)stash callback:(void (^)(NSString *))callback {
    FIRDocumentReference *docRef = [[firestore collectionWithPath:COLLECTION_STASHES] documentWithAutoID];
    [docRef setData:[stash toDocument] completion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error adding stash");
            callback(nil);
        } else {
            callback([docRef documentID]);
        }
    }];
}

/**
 * Update a stash with a user's ID. Use for if user found a stash
 * @param stash Stash to update
 * @param userId User ID that found stash
 */
- (void)updateStash:(Stash *)stash withFounderId:(NSString *)userId {
    FIRDocumentReference *docRef = [[firestore collectionWithPath:COLLECTION_STASHES] documentWithPath:[stash id]];
    [docRef updateData:@{
            @"founders_id": [FIRFieldValue fieldValueForArrayUnion:@[userId]]
    }];
}

/**
 * Retrieve a stash's media
 * @param stash Stash to retrieve media from
 * @param callback  Completion callback
 */
- (void)getMediaForStash:(Stash*)stash callback:(void (^)(NSURL*))callback {
    FIRStorageReference *reference = [[[[storage reference] child:[stash user_id]] child:[stash type]] child:[stash media_name]];
    NSURL *local = [[[[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:[stash user_id]] URLByAppendingPathComponent:[stash type]] URLByAppendingPathComponent:[stash media_name]];

    [reference writeToFile:local completion:^(NSURL *URL, NSError *error) {
        if (error) {
            NSLog(@"Error getting media with path: %@", [reference fullPath]);
        } else {
            callback(URL);
        }
    }];
}

/**
 * Add media to a stash
 * @param url NSURL for the media on the local device
 * @param stash Stash to add media to
 * @param callback Completion callback
 */
- (void)addMedia:(NSURL *)url forStash:(Stash *)stash callback:(void (^)(void))callback {
    FIRStorageReference *reference = [[[[storage reference] child:[stash user_id]] child:[stash type]] child:[stash media_name]];

    //Upload media to user's respective media storage
    [reference putFile:url metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error){
       if (error) {
           NSLog(@"Error adding media");
       } else {
           callback();
       }
    }];
}

/**
 * Parse from a dictionary object to a Stash object
 * @param documents List of NSDictionary objects
 * @return
 */
- (NSMutableArray*)parseDocuments:(NSMutableArray*)documents {
    NSMutableArray *stashes = [[NSMutableArray alloc] init];
    for (NSDictionary *data in documents) {
        Stash *stash = [Stash initWithDocument:data];
        [stashes addObject:stash];
    }

    return stashes;
}


@end