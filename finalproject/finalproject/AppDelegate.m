//
//  AppDelegate.m
//  finalproject
//
//  Created by Devin on 2018-11-20.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import "AppDelegate.h"
#import "LocationProvider.h"
#import "DataProvider.h"
#import "Stash.h"
#import <Firebase/Firebase.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //API Key - Google Maps
    [GMSServices provideAPIKey:@"AIzaSyCni8zVDGRiTCrbjcEDMjpfuohSDHRj1po"];

    //Init firebase
    [FIRApp configure];

    //TODO Remove when done testing
//    Stash *test1 = [Stash initWithDocument:@{
//            @"name": @"Test data - app",
//            @"user_id": @"Test app",
//            @"type": @"image",
//            @"difficulty": @"low",
//            @"accuracy": @"extreme",
//            @"color": @"ff00ff",
//            @"coords": [[FIRGeoPoint alloc] initWithLatitude:47.4 longitude:-52.8]
//    }];
//
//    [[DataProvider getInstance] addStash:test1 :^void(NSString* stashId) {
//        if (stashId) {
//            NSLog([NSString stringWithFormat:@"Stash added with id %s", stashId]);
//        } else {
//            NSLog(@"Stash was not added");
//        }
//    }];

//    [[DataProvider getInstance] getStashesForUser:@"devin" :^void(NSMutableArray *stash) {
//        NSLog(@"test");
//    }];

    //Initialize location provider
    [LocationProvider getInstance];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.

}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
