//
// Created by devin on 2018-11-21.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FIRGeoPoint;


@interface Stash : NSObject

@property NSString *id;
@property NSString *name;
@property NSString *type;
@property NSString *media_name;
@property NSString *user_id;
@property NSString *accuracy;
@property NSString *difficulty;
@property NSString *color;
@property FIRGeoPoint *coords;
@property NSNumber *search_radius;
@property FIRGeoPoint *search_coords;
@property NSMutableArray *founders;

+ (id)initWithDocument:(NSDictionary*)document;
- (NSDictionary*)toDocument;

- (double)getAccuracyInMeters;
- (int)getZoomLevelForDifficulty;

+ (double)getSearchCircleAlpha;
+ (double)getSearchCircleWidth;

@end