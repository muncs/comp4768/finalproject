//
//  CollectedStashesTableViewController.h
//  finalproject
//
//  Created by Justin on 2018-11-22.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Stash.h"


NS_ASSUME_NONNULL_BEGIN

@interface CollectedStashesTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
