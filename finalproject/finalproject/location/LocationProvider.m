//
// Created by Devin on 2018-11-20.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import "LocationProvider.h"


@implementation LocationProvider {

    NSMutableDictionary *listeners;
    CLLocationManager *locationManager;

}

//Singleton
static LocationProvider *instance = nil;

const int DISTANCE_FILTER = 5;

+ (id)getInstance {
    if (!instance) {
        instance = [[LocationProvider alloc] init];
    }

    return instance;
}

- (id)init {
    if (!instance) {
        instance = [super init];

        listeners = [[NSMutableDictionary alloc] init];
        locationManager = [[CLLocationManager alloc] init];

        locationManager.delegate = instance;

        [self checkLocationAuth];
    }

    return instance;
}

- (void)registerListener:(id <LocationListener>)listener withKey:(NSString *)key {
    listeners[key] = listener;
}

- (void)unregisterListenerWithKey:(NSString *)key {
    [listeners removeObjectForKey:key];
}

- (void) checkLocationAuth {
    switch (CLLocationManager.authorizationStatus) {
        //Request authorization
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"Requesting location services permissions");
            [locationManager requestWhenInUseAuthorization];
            break;
            //Do nothing
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            NSLog(@"Location services permissions denied");
            break;
            //Init location, request always-auth
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"Location services approved for when-in-use");
            [locationManager requestAlwaysAuthorization];
            [self initLocation];
            break;
            //Init location
        case kCLAuthorizationStatusAuthorizedAlways:
            NSLog(@"Location services approved for always");
            [self initLocation];
            break;
    }
}

- (void)initLocation {
    if (![locationManager locationServicesEnabled]) {
        NSLog(@"Location services disabled. Exiting...");
        return;
    }

    //Set accuracy and start receiving location updates
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = DISTANCE_FILTER;
    [locationManager startUpdatingLocation];
}

/**
 * Handle location updates
 * @param manager
 * @param locations
 */
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocationCoordinate2D coordinate = locations.lastObject.coordinate;

    NSLog([NSString stringWithFormat:@"(Long, Lat): (%f, %f)", coordinate.longitude, coordinate.latitude]);

    //Update listeners
    for (id key in listeners) {
        [listeners[key] onLocationUpdate:coordinate];
    }
}

@end