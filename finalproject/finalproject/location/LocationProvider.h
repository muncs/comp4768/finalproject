//
// Created by Devin on 2018-11-20.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationListener.h"


@interface LocationProvider : NSObject <CLLocationManagerDelegate>

+ (id) getInstance;

- (void) registerListener:(id<LocationListener>)listener withKey:(NSString*)key;

- (void) unregisterListenerWithKey:(NSString*)key;

@end