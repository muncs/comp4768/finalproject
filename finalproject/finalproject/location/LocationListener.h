
//
// Created by Devin on 2018-11-20.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol LocationListener <NSObject>

@required
- (void) onLocationUpdate:(CLLocationCoordinate2D)coordinate;

@end