//
//  MyStashesTableViewController.h
//  finalproject
//
//  Created by Justin on 2018-11-26.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyStashesTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
