//
//  MyStashesDetailViewController.m
//  finalproject
//
//  Created by Justin on 2018-12-09.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import "MyStashesDetailViewController.h"
#import "FIRGeoPoint.h"

@interface MyStashesDetailViewController ()

@end

@implementation MyStashesDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _myStashesDetailMap.userInteractionEnabled = NO;
    _myStashesDetailName.userInteractionEnabled = NO;
    _myStashesDetailPicture.userInteractionEnabled = NO;
    _myStashesDetailAccuracy.userInteractionEnabled = NO;
    _myStashesDetailDifficulty.userInteractionEnabled = NO;

    if (self.stash)
    {
        [[DataProvider getInstance] getMediaForStash:self.stash callback:^(NSURL *url) {
            NSData *imageData = [[NSData alloc] initWithContentsOfURL:url];
            _myStashesDetailPicture.image = [[UIImage alloc] initWithData:imageData];
        }];

        //Initialize camera
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[self stash] search_coords] latitude]
                                                                longitude:[[[self stash] search_coords] longitude]
                                                                     zoom:[[self stash] getZoomLevelForDifficulty]];

        //Initialize and set coords for circle
        CLLocationCoordinate2D searchCenter = CLLocationCoordinate2DMake([[[self stash] search_coords] latitude], [[[self stash] search_coords] longitude]);
        GMSCircle *circle = [GMSCircle circleWithPosition:searchCenter radius:[[[self stash] search_radius] doubleValue]];

        //Cast from hex string to int
        unsigned color = 0;
        NSScanner *scanner = [NSScanner scannerWithString:[[self stash] color]];
        [scanner scanHexInt:&color];

        //Determine RGBA
        UIColor *fillColor = [UIColor
                colorWithRed:((float)((color & 0xFF0000) >> 16)) / 255.0
                       green:((float)((color & 0x00FF00) >> 8)) / 255.0
                        blue:((float)((color & 0x0000FF) >> 0)) / 255.0
                       alpha:[Stash getSearchCircleAlpha]];

        //Configure circle
        [circle setFillColor:fillColor];
        [circle setStrokeColor:[fillColor colorWithAlphaComponent:1.0]];
        [circle setStrokeWidth:[Stash getSearchCircleWidth]];

        //Add to map
        circle.map = _myStashesDetailMap;

        //Move camera
        [_myStashesDetailMap moveCamera:[GMSCameraUpdate setCamera:camera]];

        //Display user's location
        _myStashesDetailMap.myLocationEnabled = YES;

        _myStashesDetailDifficulty.text = _stash.difficulty;
        _myStashesDetailAccuracy.text = _stash.accuracy;
        _myStashesDetailName.text = _stash.name;
    }
}

- (void)setStashModel:(Stash *)stash{
    [self setStash:stash];
}


@end
