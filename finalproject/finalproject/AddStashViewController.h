//
//  AddStashViewController.h
//  finalproject
//
//  Created by Devin on 2018-11-20.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "GoogleMaps/GoogleMaps.h"
#import "Stash.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddStashViewController : UIViewController <GMSMapViewDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *addStash;

- (IBAction)imagePicker:(UITapGestureRecognizer *)sender;
- (IBAction)cancelBtn:(UIBarButtonItem *)sender;

@property(weak, nonatomic) IBOutlet UIBarButtonItem *saveBtn;

@property(weak, nonatomic) IBOutlet UISlider *difficultySlider;
@property(weak, nonatomic) IBOutlet UILabel *difficultyLabel;

@property(weak, nonatomic) IBOutlet UILabel *accuracyLabel;
@property(weak, nonatomic) IBOutlet UISlider *accuracySlider;


@property (weak, nonatomic) IBOutlet UIImageView *stashPicture;
@property UIImage *myImage;

@property(weak, nonatomic) IBOutlet UITextField *stashName;

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate;

@end

NS_ASSUME_NONNULL_END
