//
//  AddStashViewController.m
//  finalproject
//
//  Created by Devin on 2018-11-20.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddStashViewController.h"
#import "DataProvider.h"
#import "FIRGeoPoint.h"

@interface AddStashViewController ()


@end

@implementation AddStashViewController

NSURL *imagePath;

CLLocationCoordinate2D location;

- (void)viewDidLoad {
    [super viewDidLoad];

    _stashName.delegate = self;


    self.stashPicture.image = [UIImage imageNamed:@"default"];
    self.stashPicture.userInteractionEnabled = YES;

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.latitude
                                                            longitude:location.longitude
                                                                 zoom:18];
    // Controls whether the My Location dot and accuracy circle is enabled.

    self.addStash.myLocationEnabled = YES;

//Controls the type of map tiles that should be displayed.

    self.addStash.mapType = kGMSTypeNormal;

//Shows the compass button on the map

    self.addStash.settings.compassButton = YES;

//Shows the my location button on the map

    self.addStash.settings.myLocationButton = YES;

// Set the camera

    self.addStash.camera = camera;


//Sets the view controller to be the GMSMapView delegate

    self.addStash.delegate = self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate {
    location = coordinate;
}

#pragma mark - Navigation

- (IBAction)imagePicker:(UITapGestureRecognizer *)sender {

    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init]; // Creates image picker

    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary; // Source for picker is saved photos. Only allow photos to be picked, not taken

    // Make sure the picker is notified when the user picks an image
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:true completion:nil];

}

- (IBAction)cancelBtn:(UIBarButtonItem *)sender {

}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:true completion:nil]; // Dismiss the picker if the user canceled
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    _myImage = [info objectForKey:UIImagePickerControllerOriginalImage]; // Get the selected image

    _stashPicture.image = _myImage; // Set the view to the selected image

    imagePath = info[UIImagePickerControllerImageURL];

    [self dismissViewControllerAnimated:true completion:nil]; // Dismiss the picker

}

- (IBAction)sliderValuechanged:(UISlider *)sender {
    float value = sender.value;

    self.difficultyLabel.text = [NSString stringWithFormat:@"Difficulty: %.f", value];
}

- (IBAction)accuracySliderChanged:(UISlider *)sender {
    float output = (int) sender.value;
    int newValue = (int) (3 * floor((output / 3) + 0.3));

    sender.value = newValue;

    self.accuracyLabel.text = [NSString stringWithFormat:@"Accuracy: %.f", sender.value];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender != self.saveBtn) return;

    NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];

    //TODO update accuracy and difficulty with slider values
    //TODO update color based on difficulty
    //TODO low = 00e600
    //TODO medium = ff00ff
    //TODO high = ff9933
    //TODO extreme = ff0000
    Stash *stash = [Stash initWithDocument:@{
            @"name": self.stashName.text,
            @"user_id": uniqueIdentifier,
            @"type": @"image",
            @"media_name": [imagePath lastPathComponent],
            @"difficulty": @"low",
            @"accuracy": @"extreme",
            @"color": @"ff00ff",
            @"coords": [[FIRGeoPoint alloc] initWithLatitude:location.latitude longitude:location.longitude]
    }];

    [[DataProvider getInstance] addStash:stash callback:^void(NSString *stashId) {
        if (stashId) {
            NSLog([NSString stringWithFormat:@"Stash added with id %@", stashId]);
        } else {
            NSLog(@"Stash was not added");
        }
    }];

    [[DataProvider getInstance] addMedia:imagePath forStash:stash callback:^{
        NSLog(@"Image uploaded");
    }];
}


@end