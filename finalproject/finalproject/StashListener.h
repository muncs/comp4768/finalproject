//
// Created by devin on 2018-12-03.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol StashListener <NSObject>

- (void)onStashesUpdate:(NSMutableArray*)stashes;
@end