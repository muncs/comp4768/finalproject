//
// Created by devin on 2018-11-21.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StashListener.h"

@class Stash;


@interface DataProvider : NSObject

+ (id)getInstance;

- (void)registerStashListener:(id<StashListener>)listener;

- (void)getStashesWithCallback:(void (^)(NSMutableArray*))callback;

- (void)getStashesForUser:(NSString *)userId callback:(void (^)(NSMutableArray *))callback;

- (void)getFoundStashesForUser:(NSString *)userId callback:(void (^)(NSMutableArray *))callback;

- (void)addStash:(Stash *)stash callback:(void (^)(NSString *))callback;

- (void)updateStash:(Stash*)stash withFounderId:(NSString*)userId;

- (void)getMediaForStash:(Stash*)stash callback:(void (^)(NSURL*))callback;

- (void)addMedia:(NSURL *)url forStash:(Stash *)stash callback:(void (^)(void))callback;

@end