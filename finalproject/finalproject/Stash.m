//
// Created by devin on 2018-11-21.
// Copyright (c) 2018 devinmarsh. All rights reserved.
//

#import <FirebaseFirestore/FIRGeoPoint.h>
#import "Stash.h"


@implementation Stash {

}

const NSString* KEY_ID = @"id";
const NSString* KEY_NAME = @"name";
const NSString* KEY_TYPE = @"type";
const NSString* KEY_MEDIA_NAME = @"media_name";
const NSString* KEY_USER = @"user_id";
const NSString* KEY_ACCURACY = @"accuracy";
const NSString* KEY_DIFFICULTY = @"difficulty";
const NSString* KEY_COLOR = @"color";
const NSString* KEY_COORDS = @"coords";
const NSString* KEY_SEARCH_RADIUS = @"search_radius";
const NSString* KEY_SEARCH_COORDS = @"search_coords";
const NSString* KEY_FOUNDER_IDS = @"founders_id";

+ (id)initWithDocument:(NSDictionary *)document {
    return [[self alloc] initWithDocument:document];
}

- (id)initWithDocument:(NSDictionary *)document {
    self = [super init];
    if (self) {
        NSString *id = document[KEY_ID];
        if (id) {self.id = id;}

        NSString *name = document[KEY_NAME];
        if (name) {self.name = name;}

        NSString *type = document[KEY_TYPE];
        if (type) {self.type = type;}

        NSString *media_type = document[KEY_MEDIA_NAME];
        if (media_type) {self.media_name = media_type;}

        NSString *user_id = document[KEY_USER];
        if (user_id) {self.user_id = user_id;}

        NSString *accuracy = document[KEY_ACCURACY];
        if (accuracy) {self.accuracy = accuracy;}

        NSString *difficulty = document[KEY_DIFFICULTY];
        if (difficulty) {self.difficulty = difficulty;}

        NSString *color = document[KEY_COLOR];
        if (color) {self.color = color;}

        FIRGeoPoint *coords = document[KEY_COORDS];
        if (coords) {self.coords = coords;}

        NSNumber *search_radius = document[KEY_SEARCH_RADIUS];
        if (search_radius) {self.search_radius = search_radius;}

        FIRGeoPoint *search_coords = document[KEY_SEARCH_COORDS];
        if (search_coords) {self.search_coords = search_coords;}

        self.founders = [[NSMutableArray alloc] init];
        NSMutableArray *founder_ids = document[KEY_FOUNDER_IDS];
        if (founder_ids != [NSNull null]) {self.founders = founder_ids;}
    }

    return self;
}

- (NSDictionary *)toDocument {
    NSDictionary *document = @{
        KEY_NAME: [self objectOrNull:_name],
        KEY_TYPE: [self objectOrNull:_type],
        KEY_MEDIA_NAME: [self objectOrNull:_media_name],
        KEY_USER: [self objectOrNull:_user_id],
        KEY_ACCURACY: [self objectOrNull:_accuracy],
        KEY_DIFFICULTY: [self objectOrNull:_difficulty],
        KEY_COLOR: [self objectOrNull:_color],
        KEY_COORDS: [self objectOrNull:_coords],
        KEY_SEARCH_RADIUS: [self objectOrNull:_search_radius],
        KEY_SEARCH_COORDS: [self objectOrNull:_search_coords],
        KEY_FOUNDER_IDS: [self objectOrNull:_founders]
    };

    return document;
}

/**
 * Determine accuracy radius from difficulty
 * @return Accuracy radius
 */
- (double)getAccuracyInMeters {
    if ([[self accuracy] isEqualToString:@"low"]) {
        return 9.0;
    } else if ([[self accuracy] isEqualToString:@"medium"]) {
        return 6.0;
    } else if ([[self accuracy] isEqualToString:@"high"]) {
        return 3.0;
    } else if ([[self accuracy] isEqualToString:@"extreme"]) {
        return 1.0;
    } else {
        return 0.0;
    }
}

/**
 * Determine zoom level from difficulty
 * @return Zoom level
 */
- (int)getZoomLevelForDifficulty {
    int zoomLevel = 1;

    if ([[self difficulty] isEqualToString:@"low"]) {
        zoomLevel = 19;
    } else if ([[self difficulty] isEqualToString:@"medium"]) {
        zoomLevel = 18;
    } else if ([[self difficulty] isEqualToString:@"high"]) {
        zoomLevel = 17;
    } else if ([[self difficulty] isEqualToString:@"extreme"]) {
        zoomLevel = 16;
    }

    return zoomLevel;
}

/**
 * Determine GMSCircle alpha
 * @return
 */
+ (double)getSearchCircleAlpha {
    return 0.45;
}

/**
 * Determine GMSCircle width
 * @return
 */
+ (double)getSearchCircleWidth {
    return 1;
}

/**
 * Returns null if object does not exist
 * @param object
 * @return
 */
- (id)objectOrNull:(id)object {
    return object ?: [NSNull null];
}

- (BOOL)isEqual:(id)object {
    return [[self name] isEqualToString:[((Stash*)object) name]]
        && [[self type] isEqualToString:[((Stash*)object) type]]
        && [[self media_name] isEqualToString:[((Stash*)object) media_name]]
        && [[self user_id] isEqualToString:[((Stash*)object) user_id]]
        && [[self accuracy] isEqualToString:[((Stash*)object) accuracy]]
        && [[self difficulty] isEqualToString:[((Stash*)object) difficulty]]
        && [[self color] isEqualToString:[((Stash*)object) color]]
        && [[self coords] isEqual:[((Stash*)object) coords]]
        && [[self search_coords] isEqual:[((Stash*)object) search_coords]]
        && [[self search_radius] isEqualToNumber:[((Stash*)object) search_radius]]
        && [[self founders] isEqualToArray:[((Stash*)object) founders]];
}

@end