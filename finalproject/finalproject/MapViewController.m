//
//  MapViewController.m
//  finalproject
//
//  Created by Devin on 2018-11-20.
//  Copyright © 2018 devinmarsh. All rights reserved.
//

#import "MapViewController.h"
#import "LocationProvider.h"
#import "AddStashViewController.h"
#import "DataProvider.h"
#import "Stash.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CLLocation.h>
#import <FirebaseFirestore/FIRGeoPoint.h>

@interface MapViewController ()

@end

@implementation MapViewController

NSString * const LOCATION_LISTENER_KEY = @"map";
const int ZOOM_CLOSE = 18;

NSMutableArray *stashList;
NSMutableDictionary *stashCircleMap;

int zoomLevel = ZOOM_CLOSE;

- (void)viewDidLoad {
    [super viewDidLoad];

    [[LocationProvider getInstance] registerListener:self withKey:LOCATION_LISTENER_KEY];

    stashCircleMap = [[NSMutableDictionary alloc] init];
    stashList = [[NSMutableArray alloc] init];

    [[DataProvider getInstance] registerStashListener:self];

    //Add a search circle for each stash
    [[DataProvider getInstance] getStashesWithCallback:^void(NSMutableArray *stashes) {
        for (Stash *data in stashes) {
            if (![[data user_id] isEqualToString:[[[UIDevice currentDevice] identifierForVendor] UUIDString]]) {
                [stashList addObject:data];
                [self addStashToMap:data];
            }
        }
    }];
}

- (void)loadView {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:0.0
                                                            longitude:0.0
                                                                 zoom:1];
    if (!map) {
        map = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        map.myLocationEnabled = YES;
        self.view = map;
    }
}

- (void)onLocationUpdate:(CLLocationCoordinate2D)coordinate {
    //Update latest location
    location = coordinate;

    zoomLevel = (int) [[map camera] zoom];

    //Transform to new location
    [map animateToLocation:coordinate];
    [map animateToZoom:zoomLevel];

    NSMutableArray *stashesToBeRemoved = [[NSMutableArray alloc] init];

    //Check all stashes
    for (Stash *stash in stashList) {
        if ([self hasFoundStash:stash location:coordinate]) {
            NSString *message = [NSString stringWithFormat:@"You found %@'s stash:\n %@", stash.user_id , stash.name];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Found Stash!" message:message preferredStyle:UIAlertControllerStyleAlert];

            //Action for view stash
            UIAlertAction *view = [UIAlertAction actionWithTitle:@"View Stash" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                NSLog(@"VIEW");
            }];

            //action to dismiss alert
            UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                NSLog(@"DISMISS");
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];

            //Add actions and present alert
            [alert addAction:view];
            [alert addAction:dismiss];
            [self presentViewController:alert animated:YES completion:nil];

            //Add stash to be removed later
            [stashesToBeRemoved addObject:stash];
        }
    }

    //Remove any stashes that were found
    for (Stash *stash in stashesToBeRemoved) {
        [stashCircleMap[[stash id]] setMap:nil];
        [stashList removeObject:stash];
    }
}

- (BOOL)hasFoundStash:(Stash*)stash location:(CLLocationCoordinate2D)location {
    CLLocation *stashLocation = [[CLLocation alloc] initWithLatitude:[[stash coords] latitude] longitude:[[stash coords] longitude]];
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];

    //Is current location within the stash's radius?
    if ([stashLocation distanceFromLocation:currentLocation] <= [stash getAccuracyInMeters]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender {
    UINavigationController *navController = [segue destinationViewController];
    for (UIViewController *viewController in [navController viewControllers]) {
        if ([viewController isKindOfClass:[AddStashViewController class]]) {
            [(AddStashViewController *)viewController setCoordinate:location];
            break;
        }
    }
}

- (IBAction)unwindToList:(UIStoryboardSegue *)segue {
    AddStashViewController *viewController = segue.sourceViewController;
}

- (void)addStashToMap:(Stash*)stash {
    //Validate we have all required data
    if (![stash.search_coords isEqual:[NSNull null]]
            && ![stash.search_radius isEqual:[NSNull null]]
            && ![stash.color isEqual:[NSNull null]]) {

        //Initialize and set coords for circle
        CLLocationCoordinate2D searchCenter = CLLocationCoordinate2DMake([stash.search_coords latitude], [stash.search_coords longitude]);
        GMSCircle *circle = [GMSCircle circleWithPosition:searchCenter radius:[[stash search_radius] doubleValue]];

        //Cast from hex string to int
        unsigned color = 0;
        NSScanner *scanner = [NSScanner scannerWithString:stash.color];
        [scanner scanHexInt:&color];

        //Determine RGBA
        UIColor *fillColor = [UIColor
                colorWithRed:((float)((color & 0xFF0000) >> 16)) / 255.0
                       green:((float)((color & 0x00FF00) >> 8)) / 255.0
                        blue:((float)((color & 0x0000FF) >> 0)) / 255.0
                       alpha:[Stash getSearchCircleAlpha]];

        //Configure circle
        [circle setFillColor:fillColor];
        [circle setStrokeColor:[fillColor colorWithAlphaComponent:1.0]];
        [circle setStrokeWidth:[Stash getSearchCircleWidth]];

        //Add to map
        circle.map = map;

        //Map stash to circle
        stashCircleMap[[stash id]] = circle;
    }
}

- (void)onStashesUpdate:(NSMutableArray *)stashes {
    //Remove all stashes
    [stashList removeAllObjects];

    //Remove all map circles
    for (NSString *key in stashCircleMap) {
        [stashCircleMap[key] setMap:nil];
    }

    [stashCircleMap removeAllObjects];

    //Ignore current user's stashes
    for (Stash *data in stashes) {
        if (![[data user_id] isEqualToString:[[[UIDevice currentDevice] identifierForVendor] UUIDString]]) {
            [stashList addObject:data];
            [self addStashToMap:data];
        }
    }
}


@end
